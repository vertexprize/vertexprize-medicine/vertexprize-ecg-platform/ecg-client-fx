/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.service;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertexprize.med.ecg.client.fx.app.queue.Consumer;
import org.vertexprize.med.ecg.client.fx.app.queue.EventChannel;
import org.vertexprize.med.ecg.client.fx.app.queue.EventManager;
import org.vertexprize.med.ecg.client.fx.controllers.MainController;
import org.vertexprize.medicine.ecg.data.EcgDataLoader;
import org.vertexprize.medicine.ecg.data.OptionalContainer;


/**
 * Модуль загрузки данных из файла ЭКГ
 * 
 * @author zakaryan
 */
public class EcgFileDataLoader implements EcgDataLoader<OptionalContainer<List<Double>>, File> {

      private final Logger log = LoggerFactory.getLogger(EcgFileDataLoader.class);    
    private final ExecutorService executorService = Executors.newFixedThreadPool(1);
    
  
    private final EventChannel channel;
    
    public EcgFileDataLoader() {
      log.info("Инициализация главного контроллера: "+MainController.class.getSimpleName());
      channel = EventManager.getInstance().getChannelMap().get(EcgFileDataLoader.class.getSimpleName());
      Consumer consumer = channel.getConsumer();      
      executorService.execute(consumer);
    }
    
    
    @Override
    public OptionalContainer<List<Double>> load(File source) {
  
        OptionalContainer<List<Double>> c = new OptionalContainer<>();
        
       
        return c;
    }

    
    
}
