/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.websocket.asynchttp;

import java.util.concurrent.ExecutionException;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.ws.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author developer
 */
public class AsyncWebsocketClientService {
 
    private final Logger log = LoggerFactory.getLogger(AsyncWebsocketClientService.class);    
    

    
    public void init ()
    {
        
        AsyncWebSocketEndPoint endPoint = new AsyncWebSocketEndPoint();
    
        try {
            WebSocket webSocketClient = Dsl.asyncHttpClient()
                    .prepareGet("ws://localhost:7001/cardio")
                    //.addHeader("header_name", "header_value")
                    //.addQueryParam("key", "value")
                    //.setRequestTimeout(5000)
                    .execute(endPoint.init())
                    .get();
        } catch (InterruptedException ex) {
            log.error("Ошибка подключения к серверу WS: "+ex.toString());
        } catch (ExecutionException ex) {
            log.error("Ошибка подключения к серверу WS: "+ex.toString());
        }
        
    }
    
}
