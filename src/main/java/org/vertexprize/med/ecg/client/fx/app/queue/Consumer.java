/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.app.queue;

import java.util.concurrent.BlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertexprize.medicine.ecg.data.OptionalContainer;

/**
 *
 * @author developer
 */
public class Consumer implements Runnable {
     private static final Logger log = LoggerFactory.getLogger(Consumer.class);
    private final String name;    // Имя канала 
    
    protected BlockingQueue <OptionalContainer<?>> queue = null;

    public Consumer(BlockingQueue queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    /**
     * 
     */
     @Override
    public void run() {
        try {
            OptionalContainer<?> container = queue.take();
            log.info("["+name+"] получено сообщение "+container.getValue());            
        } catch (InterruptedException ex) {
            log.error("Ошибка получения сообщения из канала["+name+"], описание ошибки:  "+ex.toString());
        }
    }
    
    
}
