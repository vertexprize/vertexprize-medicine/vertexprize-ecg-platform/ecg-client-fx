/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.app.queue;

import java.util.concurrent.BlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertexprize.medicine.ecg.data.OptionalContainer;

/**
 *
 * @author developer
 */
public class Producer {
    
    private static final Logger log = LoggerFactory.getLogger(Producer.class);
    private final String name;    // Имя канала 
    
    protected BlockingQueue <OptionalContainer<?>> queue = null;

    public Producer(BlockingQueue queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    public void send (OptionalContainer<?> c)
    {
        try {
            this.queue.put(c);
        } catch (InterruptedException ex) {
            log.error("Ошибка отправки сообщения в канал ["+name+"], описание ошибки:  "+ex.toString());
        }
    }
    
}
