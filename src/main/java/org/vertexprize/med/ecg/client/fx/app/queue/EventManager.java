/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.app.queue;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Класс для представления каналов блокирующей очереди для 
 * обмена сущностями между отдельными блоками кода, работающими в
 * разных процессах (нитях)
 *
 * @author developer
 */
public class EventManager {
    
    private static final Logger log = LoggerFactory.getLogger(EventManager.class);

    private final Map<String, EventChannel> channelMap = new ConcurrentHashMap<>(); // Карта для хранения каналов    
    private static EventManager INSTANCE;                                                  // Экземпляр EventManager

    /**
     * Метод для доступа к единственному экземпляру (шаблон singleton)
     *
     * @return
     */
    public static synchronized EventManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EventManager();
        }
        return INSTANCE;
    }

    
    /**
     * Создание канала
     *
     * @param channel
     */
    public void createChannel(EventChannel channel) {
        if (channel != null) {
            
            channelMap.putIfAbsent(channel.getName(), channel);
            log.info("Добавлен канал блокирующей очереди с именем: " + channel.getName());                        
        } else {
            log.error("Ошибка добавления канала передачи-приема событий: экземпляр канала пустой");
        }
    }

    public Map<String, EventChannel> getChannelMap() {
        return channelMap;
    }
    
    

}
