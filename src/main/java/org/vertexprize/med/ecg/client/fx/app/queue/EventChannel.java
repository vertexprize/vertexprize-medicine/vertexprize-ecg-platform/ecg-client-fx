/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.app.queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.vertexprize.medicine.ecg.data.OptionalContainer;

/**
 * 
 * @author developer
 */
public class EventChannel {
    
    private String name; // Имя канала
    private final  BlockingQueue<OptionalContainer<?>> queue = new ArrayBlockingQueue(64); // Экземпляр блокирующей очереди
    private Producer producer;
    private Consumer consumer;
    
    
    
    
    public EventChannel(String name) {
        this.name = name;
    }
    
    
    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    public BlockingQueue<OptionalContainer<?>> getQueue() {
        return queue;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    
}
