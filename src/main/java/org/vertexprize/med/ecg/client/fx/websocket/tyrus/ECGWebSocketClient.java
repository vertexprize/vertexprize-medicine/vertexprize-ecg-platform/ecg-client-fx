/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.websocket.tyrus;

import jakarta.websocket.ContainerProvider;
import jakarta.websocket.DeploymentException;
import jakarta.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertexprize.med.ecg.client.fx.service.EcgFileDataLoader;

/**
 *
 * @author developer
 */
public class ECGWebSocketClient {
    
    private final Logger log = LoggerFactory.getLogger(EcgFileDataLoader.class);    
    final static CountDownLatch messageLatch = new CountDownLatch(1);
    
    public void init() {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            String uri = "ws://localhost:7001/cardio";            
            log.info("Подключение к серверу WEBSOCKET: " + uri);
            container.connectToServer(ECGWebSocketClientEndpoint.class, URI.create(uri));
            messageLatch.await(100, TimeUnit.SECONDS);
        } catch (DeploymentException | InterruptedException | IOException ex) {
            log.error("Ошибка подключения к серверу WS: " + ex.toString());
        }
    }
}
