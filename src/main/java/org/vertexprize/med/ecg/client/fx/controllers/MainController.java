package org.vertexprize.med.ecg.client.fx.controllers;

import eu.hansolo.fx.charts.ChartType;
import eu.hansolo.fx.charts.XYChart;
import eu.hansolo.fx.charts.Axis;
import eu.hansolo.fx.charts.Grid;
import eu.hansolo.fx.charts.XYPane;
import eu.hansolo.fx.charts.data.XYChartItem;
import eu.hansolo.fx.charts.series.XYSeries;
import eu.hansolo.fx.charts.series.XYSeriesBuilder;
import eu.hansolo.fx.charts.tools.Helper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertexprize.med.ecg.client.fx.app.EcgClient;
import org.vertexprize.med.ecg.client.fx.app.queue.EventChannel;
import org.vertexprize.med.ecg.client.fx.app.queue.EventManager;
import org.vertexprize.med.ecg.client.fx.graphql.GraphqlModule;
import org.vertexprize.med.ecg.client.fx.service.FxmlLoaderSevice;
import org.vertexprize.medicine.ecg.data.OptionalContainer;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFrecord;

/**
 *
 * @author developer
 */
public class MainController {

    private final Logger log = LoggerFactory.getLogger(MainController.class);
    private Stage stage;
    private EventChannel fileloaderChannel;

    /**
    * Таблица EDF файлов
    */
    @FXML
    private TableView<EDFrecord> edfTableView = new TableView<>();
    private TableColumn<EDFrecord, String> edfFileId;              // Уникальный идентификатор обследования
    private TableColumn<EDFrecord, String> edfFileDescription;     // Описание диагностического обследования   
    private TableColumn<EDFrecord, String> edfFileType;            // Тип диагностического  обследования   
    private TableColumn<EDFrecord, String> edfFilePatientId;       // Идентификатор пациента
    
    /**
     * Модуль для хранения данных таблицы EDF файлов
     */

    private ObservableList<EDFrecord> edfData = FXCollections.observableArrayList();
    private EDFrecord selectedEdfFile;    // Текущий EDF файл, выбранный в таблице
    private int selectedEdfFileIndex;   // Индекс  EDF файла, выбранного в таблице
    
    
    /**
     * Конфигурирование таблицы
     */
    private void configureEDFfileTableView() {
        edfFileId = new TableColumn("Номер");
        edfFileId.setSortType(TableColumn.SortType.ASCENDING);
        edfFileId.setMinWidth(20);
        edfFileId.setCellValueFactory(new PropertyValueFactory<EDFrecord, String>("id"));

        edfFileDescription = new TableColumn("Описание");
        edfFileDescription.setSortType(TableColumn.SortType.ASCENDING);
        edfFileDescription.setMinWidth(20);
        edfFileDescription.setCellValueFactory(new PropertyValueFactory<EDFrecord, String>("description"));
        
        edfFileType = new TableColumn("Тип исследования");
        edfFileType.setSortType(TableColumn.SortType.ASCENDING);
        edfFileType.setMinWidth(20);
        edfFileType.setCellValueFactory(new PropertyValueFactory<EDFrecord, String>("type"));

        edfFilePatientId = new TableColumn("Пациент");
        edfFilePatientId.setSortType(TableColumn.SortType.ASCENDING);
        edfFilePatientId.setMinWidth(20);
        edfFilePatientId.setCellValueFactory(new PropertyValueFactory<EDFrecord, String>("patientId"));
        
        
        edfTableView.getColumns().setAll(
                edfFileType,
                edfFileId,
                edfFileDescription,
                edfFilePatientId
                
        );

        edfTableView.setItems(edfData);
        edfTableView.setPlaceholder(new Label("Нет загруженных исследований"));
        edfTableView.refresh();

        edfTableView.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends EDFrecord> ov, EDFrecord value, EDFrecord new_edf) -> {
                    if (new_edf != null) {
                        log.info("Выбор строки -->  [" + new_edf.getType() + ":" + new_edf.getId() + "] ");
                        selectedEdfFile = new_edf;
                    }
                }
                );

    }

    /**
     * Обновление таблицы EDF файлов
     */
    public void updateEdfFileTableView(List<EDFrecord> list) {

        Task task = new Task<Void>() {
            @Override
            public Void call() throws Exception {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        edfData.clear();
                        edfTableView.getItems().clear();
                        if (!list.isEmpty()) {
                            edfData.addAll(list);
                        }
                        edfTableView.setItems(edfData);
                        edfTableView.refresh();
                        edfTableView.getSelectionModel().selectLast();
                    }
                });
                return null;
            }
        };
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
    }
    
    
    @FXML
    private Label status;

    /**
     * Строка статуса главного окна
     */
    @FXML
    private Label statusLabel;
    
    @FXML
    /**
     * Главная панель в левой части главного экрана
     */
    private AnchorPane masterAnchorPane;

    /**
     * Верхняя панель главного экране
     */
    @FXML
    private AnchorPane upperAnchorPane;

    /**
     * Нижняя панель главного экране
     */
    @FXML
    private AnchorPane buttonAnchorPane;
    private EcgChartController chartController;
    private final GraphqlModule graphqlModule = new GraphqlModule();
    
   // private final ECGWebSocketClient wsClient = new ECGWebSocketClient();
   // private final AsyncWebsocketClientService websocketClient = new AsyncWebsocketClientService();
    
    @FXML
    private TreeView EcgTreeView ;
    private TreeItem<String> ecgDatabaseTreeView = new TreeItem<>("База данных кардиограмм");
    private TreeItem<String> cardiographTreeView = new TreeItem<>("Список кардиографов");
    
    public void init() {
        
        
        log.info("Инициализация главного контроллера: " + MainController.class.getSimpleName());
        log.info("Создание каналов для передачи событий ...");
        log.info("Список каналов [" + EventManager.getInstance().getChannelMap().size() + "] записей: " + EventManager.getInstance().getChannelMap().keySet());

        TreeItem<String> root = new TreeItem<>("Кардиология");
        TreeItem<String> c1 = new TreeItem<>("Кардиограф-1 [192.168.17.20]");
        TreeItem<String> c2 = new TreeItem<>("Кардиограф-2 [192.168.17.21]");

        root.getChildren().add(ecgDatabaseTreeView);
        root.getChildren().add(cardiographTreeView);
        
        configureEDFfileTableView();
        
        cardiographTreeView.getChildren().add(c1);
        cardiographTreeView.getChildren().add(c2);
        
        EcgTreeView.setRoot(root);
        
        
        //wsClient.init();
        //websocketClient.init();
        graphqlModule.init();             

    }

    public void setChartController(EcgChartController chartController) {
        this.chartController = chartController;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    

    @FXML
    private void showEcgLineChart() throws IOException {
        statusLabel.setText("Размещение графика ЭКГ в верней панели ...");

        LineChart ecgChart = FxmlLoaderSevice.loadLineChart("ecg-line-chart");
        //upperAnchorPane.getChildren().add(ecgChart);    

        if (ecgChart != null) {
            ListView list = new ListView();
            AnchorPane.setTopAnchor(list, 10.0);
            AnchorPane.setLeftAnchor(list, 10.0);
            AnchorPane.setRightAnchor(list, 65.0);
            // Button will float on right edge
            Button button = new Button("Add");
            AnchorPane.setTopAnchor(button, 10.0);
            AnchorPane.setRightAnchor(button, 10.0);

            AnchorPane.setTopAnchor(ecgChart, 10.0);
            upperAnchorPane.getChildren().addAll(list, button, ecgChart);

        } else {
            System.out.println("График не загружен");
        }
    }
    
    @FXML
    private void connectToServer() throws IOException {
        log.info("Подключение к серверу ... для выполнения запроса Graphql ");

        String request = "query EdfFilesByDiagnosticType {\n"
                + "    edfFilesByDiagnosticType {\n"
                + "        id\n"
                + "        description\n"
                + "        type\n"
                + "        patientId\n"
                + "    }\n"
                + "}";

        try {
            OptionalContainer<List<EDFrecord>> result = graphqlModule.executeHttpRequest(request);
            if (result.isProcessed())
            {
                
                log.info("Инициализация обновления таблицы EDF ...");
                updateEdfFileTableView(result.getValue());
            }
            
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            java.util.logging.Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }



    @FXML
    private void showMultiTimeSeriesChart() throws IOException {
        statusLabel.setText("Размещение графиков ЭКГ в верней панели ...");

        String chartName = "ecg-multi-time-series-chart";

        LineChart ecgChart = FxmlLoaderSevice.loadLineChart(chartName);
        //upperAnchorPane.getChildren().add(ecgChart);    

        if (ecgChart != null) {           
            initMultiTimeSeriesChart();
        } else {
            log.error("Не удалось загрузить шаблон графиков ЭКГ из файла: " + chartName);
        }
    }

    /**
     * Инициализация множества графиков
     */
    private  void initMultiTimeSeriesChart() {
        
        final Double AXIS_WIDTH = 25d;
        
        XYChart<XYChartItem> multiTimeSeriesChart;
        Axis xAxis;
        Axis yAxis;

        // Data Series 1
        List<XYSeries<XYChartItem>> listOfSeries1 = new ArrayList<>();
        String filename1 = EcgClient.class.getResource("data1.csv").toExternalForm().replaceAll("file:", "");
        String data1 = Helper.readTextFile(filename1);
        String[] lines1 = data1.split(System.getProperty("line.separator"));
        String firstLine1 = lines1[0];
        String[] names1 = firstLine1.split(",");
        List<Double> xAxisValues1 = new ArrayList<>();
        double yAxisMinValue1 = Double.MAX_VALUE;
        double yAxisMaxValue1 = Double.MIN_VALUE;
        Map<String, List<XYChartItem>> seriesDataMap1 = new HashMap<>();
        for (int i = 1; i < lines1.length; i++) {
            String line = lines1[i];
            List<XYChartItem> xyItems = new ArrayList<>();
            String[] dataPoints = line.split(",");
            double timePoint = Double.parseDouble(dataPoints[0]);
            xAxisValues1.add(timePoint);
            for (int j = 1; j < dataPoints.length; j++) {
                double value = Double.parseDouble(dataPoints[j]);
                yAxisMinValue1 = Math.min(yAxisMinValue1, value);
                yAxisMaxValue1 = Math.max(yAxisMaxValue1, value);

                if (seriesDataMap1.containsKey(names1[j])) {
                    seriesDataMap1.get(names1[j]).add(new XYChartItem(timePoint, value, names1[j], Color.MAGENTA));
                } else {
                    seriesDataMap1.put(names1[j], new LinkedList<>());
                    seriesDataMap1.get(names1[j]).add(new XYChartItem(timePoint, value, names1[j], Color.MAGENTA));
                }
            }
        }

        seriesDataMap1.entrySet().forEach(entry -> {
            XYSeries<XYChartItem> xySeries = XYSeriesBuilder.create()
                    .items(entry.getValue().toArray(new XYChartItem[0]))
                    .chartType(ChartType.MULTI_TIME_SERIES)
                    .fill(Color.TRANSPARENT)
                    .stroke(Color.MAGENTA)
                    .symbolFill(Color.RED)
                    .symbolStroke(Color.TRANSPARENT)
                    .symbolsVisible(false)
                    .symbolSize(5)
                    .strokeWidth(0.5)
                    .build();
            listOfSeries1.add(xySeries);
        });

        // Data Series 2
        List<XYSeries<XYChartItem>> listOfSeries2 = new ArrayList<>();
        String filename2 = EcgClient.class.getResource("data2.csv").toExternalForm().replaceAll("file:", "");
        String data2 = Helper.readTextFile(filename2);
        String[] lines2 = data2.split(System.getProperty("line.separator"));
        String firstLine2 = lines2[0];
        String[] names2 = firstLine2.split(",");
        List<Double> xAxisValues2 = new ArrayList<>();
        double yAxisMinValue2 = Double.MAX_VALUE;
        double yAxisMaxValue2 = Double.MIN_VALUE;
        Map<String, List<XYChartItem>> seriesDataMap2 = new HashMap<>();
        for (int i = 1; i < lines2.length; i++) {
            String line = lines2[i];
            List<XYChartItem> xyItems = new ArrayList<>();
            String[] dataPoints = line.split(",");
            double timePoint = Double.parseDouble(dataPoints[0]);
            xAxisValues2.add(timePoint);
            for (int j = 1; j < dataPoints.length; j++) {
                double value = Double.parseDouble(dataPoints[j]);
                yAxisMinValue2 = Math.min(yAxisMinValue2, value);
                yAxisMaxValue2 = Math.max(yAxisMaxValue2, value);

                if (seriesDataMap2.containsKey(names2[j])) {
                    seriesDataMap2.get(names2[j]).add(new XYChartItem(timePoint, value, names2[j], Color.MAGENTA));
                } else {
                    seriesDataMap2.put(names2[j], new LinkedList<>());
                    seriesDataMap2.get(names2[j]).add(new XYChartItem(timePoint, value, names2[j], Color.MAGENTA));
                }
            }
        }

        seriesDataMap2.entrySet().forEach(entry -> {
            XYSeries<XYChartItem> xySeries = XYSeriesBuilder.create()
                    .items(entry.getValue().toArray(new XYChartItem[0]))
                    .chartType(ChartType.MULTI_TIME_SERIES)
                    .fill(Color.TRANSPARENT)
                    .stroke(Color.MAGENTA)
                    .symbolFill(Color.RED)
                    .symbolStroke(Color.TRANSPARENT)
                    .symbolsVisible(false)
                    .symbolSize(5)
                    .strokeWidth(0.5)
                    .build();
            listOfSeries2.add(xySeries);
        });

        // Data Series 3
        List<XYSeries<XYChartItem>> listOfSeries3 = new ArrayList<>();
        String filename3 = EcgClient.class.getResource("data3.csv").toExternalForm().replaceAll("file:", "");
        String data3 = Helper.readTextFile(filename3);
        String[] lines3 = data3.split(System.getProperty("line.separator"));
        String firstLine3 = lines3[0];
        String[] names3 = firstLine3.split(",");
        List<Double> xAxisValues3 = new ArrayList<>();
        double yAxisMinValue3 = Double.MAX_VALUE;
        double yAxisMaxValue3 = Double.MIN_VALUE;
        Map<String, List<XYChartItem>> seriesDataMap3 = new HashMap<>();
        for (int i = 1; i < lines3.length; i++) {
            String line = lines3[i];
            List<XYChartItem> xyItems = new ArrayList<>();
            String[] dataPoints = line.split(",");
            double timePoint = Double.parseDouble(dataPoints[0]);
            xAxisValues3.add(timePoint);
            for (int j = 1; j < dataPoints.length; j++) {
                double value = Double.parseDouble(dataPoints[j]);
                yAxisMinValue3 = Math.min(yAxisMinValue3, value);
                yAxisMaxValue3 = Math.max(yAxisMaxValue3, value);

                if (seriesDataMap3.containsKey(names3[j])) {
                    seriesDataMap3.get(names3[j]).add(new XYChartItem(timePoint, value, names3[j], Color.MAGENTA));
                } else {
                    seriesDataMap3.put(names3[j], new LinkedList<>());
                    seriesDataMap3.get(names3[j]).add(new XYChartItem(timePoint, value, names3[j], Color.MAGENTA));
                }
            }
        }

        seriesDataMap3.entrySet().forEach(entry -> {
            XYSeries<XYChartItem> xySeries = XYSeriesBuilder.create()
                    .items(entry.getValue().toArray(new XYChartItem[0]))
                    .chartType(ChartType.MULTI_TIME_SERIES)
                    .fill(Color.TRANSPARENT)
                    .stroke(Color.MAGENTA)
                    .symbolFill(Color.RED)
                    .symbolStroke(Color.TRANSPARENT)
                    .symbolsVisible(false)
                    .symbolSize(5)
                    .strokeWidth(0.5)
                    .build();
            listOfSeries3.add(xySeries);
        });

        double yAxisMinValue = Math.min(Math.min(yAxisMinValue1, yAxisMinValue2), yAxisMinValue3);
        double yAxisMaxValue = Math.max(Math.max(yAxisMaxValue1, yAxisMaxValue2), yAxisMaxValue3);

        // MultiTimeSeriesChart
        double start = xAxisValues1.stream().min(Comparator.comparingDouble(Double::doubleValue)).get();
        double end = xAxisValues1.stream().max(Comparator.comparingDouble(Double::doubleValue)).get();
        xAxis = Helper.createBottomAxis(start, end, "Time [s]", true, AXIS_WIDTH);
        xAxis.setDecimals(1);

        yAxis = Helper.createLeftAxis(yAxisMinValue, yAxisMaxValue, "Ratio", true, AXIS_WIDTH);
        yAxis.setDecimals(2);

        xAxis.setZeroColor(Color.BLACK);
        yAxis.setZeroColor(Color.BLACK);

        XYPane xyPane1 = new XYPane(listOfSeries1);
        xyPane1.setAverageStroke(Color.rgb(247, 118, 109));
        xyPane1.setAverageStrokeWidth(3);
        xyPane1.setStdDeviationFill(Color.rgb(247, 118, 109, 0.2));
        xyPane1.setStdDeviationStroke(Color.rgb(120, 120, 120));
        xyPane1.setEnvelopeVisible(true);
        xyPane1.setEnvelopeFill(Color.TRANSPARENT);
        xyPane1.setEnvelopeStroke(Color.rgb(247, 118, 109));

        XYPane xyPane2 = new XYPane(listOfSeries2);
        xyPane2.setAverageStroke(Color.rgb(42, 186, 56));
        xyPane2.setAverageStrokeWidth(3);
        xyPane2.setStdDeviationFill(Color.rgb(42, 186, 56, 0.2));
        xyPane2.setStdDeviationStroke(Color.rgb(120, 120, 120));
        xyPane2.setEnvelopeVisible(true);
        xyPane2.setEnvelopeFill(Color.TRANSPARENT);
        xyPane2.setEnvelopeStroke(Color.rgb(42, 186, 56));

        XYPane xyPane3 = new XYPane(listOfSeries3);
        xyPane3.setAverageStroke(Color.rgb(97, 155, 255));
        xyPane3.setAverageStrokeWidth(3);
        xyPane3.setStdDeviationFill(Color.rgb(97, 155, 255, 0.2));
        xyPane3.setStdDeviationStroke(Color.rgb(120, 120, 120));
        xyPane3.setEnvelopeVisible(true);
        xyPane3.setEnvelopeFill(Color.TRANSPARENT);
        xyPane3.setEnvelopeStroke(Color.rgb(97, 155, 255));

        List<XYPane> xyPanes = new ArrayList<>();
        xyPanes.add(xyPane1);
        xyPanes.add(xyPane2);
        xyPanes.add(xyPane3);

        multiTimeSeriesChart = new XYChart(xyPanes, yAxis, xAxis);

        Grid grid1 = new Grid(xAxis, yAxis);
        multiTimeSeriesChart.setGrid(grid1);

        StackPane pane = new StackPane(multiTimeSeriesChart);
        pane.setPadding(new Insets(10));
        
        AnchorPane.setTopAnchor(pane, 0.0);
        AnchorPane.setBottomAnchor(pane, 0.0);
        AnchorPane.setLeftAnchor(pane, 0.0);
        AnchorPane.setRightAnchor(pane, 0.0);
        
        
        upperAnchorPane.getChildren().addAll(pane);
        
    }

    @FXML
    private void showEcgTableView() throws IOException {
        statusLabel.setText("Размещение таблицы в нижней панели ...");

        TableView ecgTable = FxmlLoaderSevice.loadTableView("ecg-table-view");

        if (ecgTable != null) {
            ListView list = new ListView();
            AnchorPane.setTopAnchor(list, 10.0);
            AnchorPane.setLeftAnchor(list, 10.0);
            AnchorPane.setRightAnchor(list, 65.0);
            // Button will float on right edge
            Button button = new Button("Add");
            AnchorPane.setTopAnchor(button, 10.0);
            AnchorPane.setRightAnchor(button, 10.0);

            AnchorPane.setTopAnchor(ecgTable, 10.0);
            buttonAnchorPane.getChildren().addAll(list, button, ecgTable);

        } else {
            System.out.println("Таблица не загружена");
        }
    }

    @FXML
    private void exitApplication() throws IOException {
        statusLabel.setText("Завершение работы приложения...");

        Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION);
        exitAlert.setTitle("Выход из приложения");
        exitAlert.setHeaderText(null);
        exitAlert.setContentText("Хотите завершить приложение ?");

        ButtonType yesBtn = new ButtonType("Да", ButtonBar.ButtonData.YES);
        ButtonType noBtn = new ButtonType("Нет", ButtonBar.ButtonData.NO);

        exitAlert.getButtonTypes().setAll(yesBtn, noBtn);
        exitAlert.showAndWait();
        if (exitAlert.getResult().getButtonData() == ButtonBar.ButtonData.YES) {

            Platform.exit();
            System.exit(0);
        }

    }

    /**
     * Загрузка данных из файла c использованием диалога
     */
    @FXML
    private void loadDataFromFile() {
        log.info("Открытие файла ...");
        
        statusLabel.setText("Открытие файла кардиограммы...");
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Открыть файл кардиограммы");
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            if (file.exists()) {
                if (file.canRead()) {
                    log.info("Получен доступ к файлу: "+file.getAbsolutePath());
                    updateStatusLabel("Получен доступ к файлу: " + file.getAbsolutePath());
                    updateCardioDatabase(file);
                  
                    
                    OptionalContainer<File> c = new OptionalContainer();
                    c.setValue(file);                                                          
                } else {
                    updateStatusLabel("Ошибка доступа к файлу: " + file.getName() + "  отсутствуют права на операцию чтения файла");
                }
            } else {
                updateStatusLabel("Ошибка: не существует файл: " + file.getName() + " каталог: " + file.getParent());
            }
        } else {
            updateStatusLabel("Ошибка: файл не выбран");
        }
    }

    
    /**
     * Обновление списка кардиограмм
     */
    private void updateCardioDatabase(File file)
    {
        
         if (file != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                   
                    ecgDatabaseTreeView.getChildren().add( new TreeItem<>(file.getName()));
                    
                }
            });
        }
    }       
    
    
    
    /**
     * Обновление строки статуса приложения
     */
    private void updateStatusLabel(String status) {
        if (status != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    statusLabel.setText(status);
                }
            });
        }
    }

//    /**
//     * Отображение графика ( не работает - нужна отладка и документация)
//     *
//     * @throws IOException
//     * @deprecated
//     */
//    @FXML
//    @Deprecated
//    private void showTreeView() throws IOException {
//        statusLabel.setText("Размещение treeview в левой панели ...");
//
//        File file = new File("/home/zakaryan");
//        String filename = file.getName();
//
//        TreeItem<String> rootItem = createItems(file);
//
//        TreeView<String> tree = new TreeView<String>(rootItem);
//        masterAnchorPane.getChildren().addAll(tree);
//    }

   

}
