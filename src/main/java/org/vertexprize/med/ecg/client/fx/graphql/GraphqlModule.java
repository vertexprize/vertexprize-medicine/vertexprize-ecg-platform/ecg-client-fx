/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.graphql;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.graphql.dgs.client.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertexprize.medicine.ecg.data.OptionalContainer;
import org.vertexprize.medicine.ecg.medical.diagnostics.EDFrecord;

/**
 *
 * @author vaganovdv
 */
public class GraphqlModule {

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private WebClient webClient;
    private WebClientGraphQLClient client;
    private ObjectMapper mapper = JsonMapper.builder()
            .addModule(new ParameterNamesModule())
            .addModule(new Jdk8Module())
            .addModule(new JavaTimeModule())
            .enable(SerializationFeature.INDENT_OUTPUT)
            .build();

    private final Logger log = LoggerFactory.getLogger(GraphqlModule.class);

    public void init() {

        log.info("Инициазация Webclient GraphQL...");

        //Configure a WebClient for your needs, e.g. including authentication headers and TLS.
        webClient = WebClient.create("http://localhost:7779/graphql");
        client = MonoGraphQLClient.createWithWebClient(webClient);

//    ObjectMapper mapper = new ObjectMapper();
//    
        ////
//    Mono<GraphQLResponse> graphQLResponseMono = client.reactiveExecuteQuery(query);
//
//    //GraphQLResponse has convenience methods to extract fields using JsonPath.
//    Mono<String> somefield = graphQLResponseMono.map(r ->  r.extractValue("somefield"));
//
//    
//    
//    //Don't forget to subscribe! The request won't be executed otherwise.
//    somefield.subscribe();

        log.info("Инициазация Webclient ЗАВЕРШЕНА");

    }

    /**
     * Выполнение запроса graphQL
     *
     * @param document
     * @return
     */
    public OptionalContainer<List<EDFrecord>> executeHttpRequest(String request) throws InterruptedException, ExecutionException {
        OptionalContainer<List<EDFrecord>> c = new OptionalContainer<>();

        log.info("Выполнение запроса graphql ...");

        Task<List<EDFrecord>> task = new Task<List<EDFrecord>>() {
            @Override
            protected List<EDFrecord> call() throws Exception {

                List<EDFrecord> list = new ArrayList<>();

                if (request != null && !request.isEmpty()) {

                    Mono<GraphQLResponse> reactiveExecuteQuery = client.reactiveExecuteQuery(request);
                    reactiveExecuteQuery.subscribe();

                    log.info("Запрос...");

                    GraphQLResponse responce = reactiveExecuteQuery.block();

                    log.info("Обработка ответа...");

                    if (responce.hasErrors()) {
                        responce.getErrors().stream().forEach(e -> {
                            c.getErrors().add(e.toString());
                            log.error(e.toString());
                        });
                    } else {
                        log.info("Получение результатов...");
                        Map<String, Object> data = responce.getData();

                        Set<String> keySet = data.keySet();
                        log.info("Список результатов: " + keySet);

                        Object obj = data.get("edfFilesByDiagnosticType");

                        String d = obj.toString();
                        log.info("Получены данные: " + d);

                        TypeReference<List<EDFrecord>> ref = new TypeReference<>() {
                        };

                        try {
                            list = mapper.readValue(mapper.writeValueAsString(obj), ref);
                            log.info("Прочитан список [" + list.size() + "]  объектов ");

                            StringBuilder sb = new StringBuilder();
                            sb.append("\n");
                            list.stream().forEach(edf -> {
                                sb.append(edf).append("\n");

                            });

                            log.info(sb.toString());

                        } catch (JsonProcessingException ex) {
                            log.error("Ошибка JSON: " + ex.toString());
                        }

                    }
                }
                return list;
            }
        };

        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();

        List<EDFrecord> list = task.get();
        if (list != null && !list.isEmpty()) {

            log.info("Сформирован список [" + list.size() + "] edf файлов для обновления таблицы");
            c.setValue(list);
            c.setProcessed(true);
        }

        return c;
    }

}
