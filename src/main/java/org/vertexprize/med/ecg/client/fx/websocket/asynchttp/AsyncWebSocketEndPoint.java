/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.med.ecg.client.fx.websocket.asynchttp;

import org.asynchttpclient.ws.WebSocket;
import org.asynchttpclient.ws.WebSocketListener;
import org.asynchttpclient.ws.WebSocketUpgradeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author developer
 */
public class AsyncWebSocketEndPoint {

    private final Logger log = LoggerFactory.getLogger(AsyncWebSocketEndPoint.class);

    public WebSocketUpgradeHandler init() {
        WebSocketUpgradeHandler.Builder upgradeHandlerBuilder = new WebSocketUpgradeHandler.Builder();

        WebSocketUpgradeHandler wsHandler = upgradeHandlerBuilder
                .addWebSocketListener(new WebSocketListener() {
                    @Override
                    public void onOpen(WebSocket websocket) {
                        log.info("Подключено к серверу WS: address = " + websocket.getRemoteAddress());
                        websocket.sendTextFrame("Подключен клиент [ecg-client-fx] ip = "+websocket.getLocalAddress());
                    }
                    @Override
                    public void onClose(WebSocket websocket, int code, String reason) {

                        log.info("Отключено от сервера WS: address = " + websocket.getRemoteAddress());
                    }
                    @Override
                    public void onError(Throwable t) {
                        log.error("Ошибка WEBSOCKET: " + t.getMessage());
                    }
                }).build();
        return wsHandler;

    }
}
