package org.vertexprize.med.ecg.client.fx.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.vertexprize.med.ecg.client.fx.controllers.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import org.vertexprize.med.ecg.client.fx.app.queue.EventChannel;
import org.vertexprize.med.ecg.client.fx.app.queue.EventManager;
import org.vertexprize.med.ecg.client.fx.service.EcgFileDataLoader;

/**
 * Главный класс приложения
 *
 * JavaFX EcgClient
 */
public class EcgClient extends Application {

    private final Logger log = LoggerFactory.getLogger(EcgClient.class);

    private static Scene scene;
    private Stage stage;

    //  private static Vertx vertx;         // Экземпляр Vertx    
    private static EcgClient INSTANCE;  // Экземпляр EcgClient

    private final String appName = "Клиент облачной системы кардиологических исследований";
    private final String appVersion = "Версия 0.0.17";
    private MainController controller;

    @Override
    public void start(Stage stage) throws IOException {
        log.info("Старт приложения ...");

        this.stage = stage;
        scene = new Scene(loadFXML("main"), 1024, 800);
        stage.setScene(scene);

        // InternalLoggerFactory.setDefaultFactory(Log4J2LoggerFactory.INSTANCE);
        stage.setTitle(appName + " " + appVersion);
        stage.setResizable(false);
        stage.setOnCloseRequest(closeRequest -> {

            Alert exitAlert = new Alert(AlertType.CONFIRMATION);
            exitAlert.setTitle("Выход из приложения");
            exitAlert.setHeaderText(null);
            exitAlert.setContentText("Хотите завершить приложение ?");

            ButtonType yesBtn = new ButtonType("Да", ButtonData.YES);
            ButtonType noBtn = new ButtonType("Нет", ButtonData.NO);

            exitAlert.getButtonTypes().setAll(yesBtn, noBtn);
            exitAlert.showAndWait();

            if (exitAlert.getResult().getButtonData() == ButtonData.YES) {
                Platform.exit();
                System.exit(0);
            } else {
                closeRequest.consume();
            }
        });

        stage.show();

    }

    private void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private Parent loadFXML(String fxml) {
        FXMLLoader loader = new FXMLLoader(EcgClient.class.getResource(fxml + ".fxml"));
        Parent root = null;
        try {
            root = (Parent) loader.load();
            log.info("Загрузка файла FXML: " + loader.getLocation());

            controller = (MainController) loader.getController();
            controller.setStage(stage);
            controller.init();

        } catch (IOException ex) {
            log.error("Ошибка загрузки fxml файла: " + ex.toString());
        }

        return root;
    }

    public static void main(String[] args) {
        createEventChannels();
        launch();
    }

    private static void createEventChannels() {

        EventChannel fileLoader = new EventChannel(EcgFileDataLoader.class.getSimpleName());
        EventManager.getInstance().createChannel(fileLoader);

        int size = EventManager.getInstance().getChannelMap().size();
        System.out.println("Создано [" + size + "] каналов передачи сообщений");

    }

}
